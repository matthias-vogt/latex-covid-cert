# Credit card sized COVID-Certificate in LaTeX

A LaTeX Template to make a credit card sized COVID-certificate that you can stick on the back of your ID. The example below is a German version but it can be adapted to your language. There are a few different language versions in the [src](./src/) folder already, including English/German and English/French.

## Example

![The front of the card](./covid-cert.png)

This image was made using [`covid-cert-de-en-template.tex`](https://gitlab.com/matthias-vogt/latex-covid-cert/-/raw/main/src/covid-cert-de-en-template.tex).

## Getting started

1. **Screenshot your QR code** to get it as an image file. To do this, you can get a PDF of your EU Digital COVID Certificate from the [CovPass App](https://www.digitaler-impfnachweis-app.de/en) or from the [Corona-Warn-App](https://www.coronawarn.app/) where also the rest of your vaccination information is given.
    1. Put the image in the same directory as your chosen .tex file.
    2. Update line 30 in your chosen .tex file where it says `\newcommand{\qrCodeFileName}{} % Ex. code.png`, and insert the file name inside the second set of brackets.
3. **Insert the rest of your information** in the block where it says `ENTER YOUR PERSONAL INFORMATION HERE` between lines 18 and 30.
4. **Run XeLaTeX** by executing the command `xelatex <filename>.tex` in the `src` directory on your Terminal. You can also compile the file with XeLaTex using TeXstudio. If you don't have LaTeX installed, you can [refer to this installation page](https://www.latex-project.org/get/).
5. Print out the PDF, cut it, and laminate it matte. It's super nice.
6. Tape the card to the back of your ID for easy checking.

## Contributing

Contributions are welcome :)

### Contributors
| [<img src="https://gitlab.com/uploads/-/system/user/avatar/8644781/avatar.png?width=100" width="100px;"/><br /><sub><b>Hussein Esmail</b></sub>](https://gitlab.com/hussein-esmail7) |
| :---: |



## License

This project is licensed under the MIT license.
